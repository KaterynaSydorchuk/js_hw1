let a = 5; 

console.log(typeof a); 

let name = 'Kate'; 

let lastName = 'Sydorchuk'; 

console.log('My name is ' + name, '' + lastName); 

let c = 10; 

console.log(`${a}`); 

/* Теоретичні питання
 1. Як можна оголосити змінну у Javascript?
    За допомогою трьох ключових слів: let const var
 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
    За допомогою лапок (кавичок): Використання одинарних лапок (' ') та використання подвійних лапок (" "). Створення за допомогою конструктора String let a = String("Hello");
 3. Як перевірити тип даних змінної в JavaScript?
    За допомогою typeof, наприклад console.log(typeof true) видасть тип boolean
 4. Поясніть чому '1' + 1 = 11.
    тому що 1 - це тип даних number, а '1' це тип даних string, тому 1 перетвориться в тип string і результатом буде тип string 
*/
 
